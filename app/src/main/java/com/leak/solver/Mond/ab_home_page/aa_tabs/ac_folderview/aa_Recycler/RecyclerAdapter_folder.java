package com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.FileManager_Home;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.paracelable_Folder_VideoPlayer;
import com.leak.solver.Mond.ac_VideoPlayer.VideoPlayer;
import com.leak.solver.Mond.zc_Glide.FutureStudioAppGlideModule;
import com.leak.solver.Mond.zz_Config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter_folder extends RecyclerView.Adapter<ViewHolder_folder>
{

    private List<Directory_folder> directories;
    private Context context;

     VideoPlayerThread  videoPlayerThread=null;



    public RecyclerAdapter_folder(List<Directory_folder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;


    }

    public void setRecyclerAdapter(List<Directory_folder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder_folder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ad_d_folder_recycler_item, viewGroup, false);


        return new ViewHolder_folder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder_folder viewHolderFolder, final int i)
    {


        Directory_folder directory = this.directories.get(i);
     //   viewHolderFolder.video_ref.setText(directory.video_thumb);


        String string_path = directory.file_name;
        String[] parts = string_path.split(".");
        // viewHolder.file_name.setText(parts[parts.length-1]);
        //viewHolderFolder.file_name.setText("nono");
        //viewHolderFolder.file_name.setText( string_path.substring(0, string_path.lastIndexOf(".")));
        viewHolderFolder.file_name.setText( string_path.substring(0, string_path.lastIndexOf(".")));
        long interval = 1 * 200;
        //RequestOptions options = new RequestOptions().frame(interval);

        RequestOptions options = new RequestOptions();
        options.fitCenter();

        // int width = 200;
        // int height = 200;
        // LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width,height);
        //  viewHolder.video_thumb.setLayoutParams(parms);

        FutureStudioAppGlideModule futureStudioAppGlideModule = new FutureStudioAppGlideModule();



        futureStudioAppGlideModule.add_image_to_view(context,viewHolderFolder,Uri.fromFile( new File( directory.video_thumb ) ));


        // Directory_folder directoryFolder = this.directories.get(i);
      //  viewHolderFolder.file_name.setText(directoryFolder.file_name);


      //  List<String> values = new ArrayList<String>();
//        values = ((MyApplication) (MyApplication.getContext())).map.get(((MyApplication) (MyApplication.getContext())).names_fav.get(i));



        viewHolderFolder.slice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {



                List<String> values = new ArrayList<String>();

                //   values = ((aa_activity_home_screen) context)._global_StartUP.ab_fileManagerStart.names_fav.get(i);

                Directory_folder directory_folder =  ((aa_activity_home_screen) context).fileManager_home_folder.directories_folder.get(i);

                // Paramter_Parcelable paramter_parcelable=null;
                //   paramter_parcelable = new Paramter_Parcelable(values.get(0),values.get(1));
                //  ((MyApplication) (MyApplication.getContext())).map.clear();
                //     ((MyApplication) (MyApplication.getContext())).names_fav.clear();


                paracelable_Folder_VideoPlayer paracelableFolderVideoPlayer =new paracelable_Folder_VideoPlayer(0,directory_folder.video_thumb,directory_folder.file_name,null,i);
             //   new VideoPlayerThread().start();
               // paracelableFolderVideoPlayer.mContext=aa_activity_home_screen.getContext();
              //  paracelableFolderVideoPlayer.CurrentIndex=i;

              //  paracelableFolderVideoPlayer.

              //  video_index
//
                Intent intent = new Intent(aa_activity_home_screen.mContext, VideoPlayer.class);
                intent.putExtra("VideoData", paracelableFolderVideoPlayer);

                if(zz_Config.TEST)
                {


                 //   Log.d("_##YES","from intent"+paracelableFolderVideoPlayer.mContext+paracelableFolderVideoPlayer.CurrentIndex  );


                }
                aa_activity_home_screen.getContext().BY_iNTENT=true;

                aa_activity_home_screen.getContext().startActivity(intent);

              //  aa_activity_home_screen.mContext.fileManager_home.videoPlayerThread = new  aa_activity_home_screen.mContext.fileManager_home.VideoPlayerThread()

            //   aa_activity_home_screen.mContext.fileManager_home.videoPlayerThread.start();

/*
                 Paramter_Parcelable paramter_parcelable=null;
                 paramter_parcelable = new Paramter_Parcelable(values.get(0),values.get(1));

*/


            }
        });







    }


    public static class VideoPlayerThread extends Thread {
        @Override
        public void run()
        {

            if(zz_Config.TEST)
                Log.d("_####_CALL_VIDEO", " Inside VideoPlayerThread Thread :  aa_activity_StartUp A");




          //  aa_activity_home_screen.mContext.fileManager_home.intent = new Intent(aa_activity_home_screen.mContext, VideoPlayer.class);

           // aa_activity_home_screen.mContext.fileManager_home.intent.putExtra()
         //   aa_activity_home_screen.mContext.startActivity(aa_activity_home_screen.mContext.fileManager_home.intent);





////////


            if(zz_Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "Video_redirect()B1 : aa_activity_StartUp");








            if(zz_Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "Video_redirect()B2 : aa_activity_StartUp");


            ///////////////////////////////////////////////////////////////////////////////////////////



            if(zz_Config.TEST)
                Log.d("_####_CALL_VIDEO", " Inside VideoPlayerThread Thread :  aa_activity_StartUp B");


        }

    }


    @Override
    public int getItemCount()
    {
        return directories.size();
    }
}
