package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_home_screen_PagerAdapter;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler.DirectoryFav;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapterFav;
import com.leak.solver.Mond.zz_Config;

import java.util.ArrayList;
import java.util.List;

import static com.leak.solver.Mond.ab_home_page.aa_activity_home_screen.getContext;

public class FileManager_Home_fav {




    ///fav folder




    public RecyclerView recyclerViewFav=null;
    public RecyclerAdapterFav recyclerAdapterFav=null;
    public List<DirectoryFav> directories =null;
    public SwipeRefreshLayout mSwipeRefreshLayoutFav=null;






    public Thread Thread_Full_and_Half_FAV =null;




    Handler directory_handler =null;




    public Boolean READY_TO_UPDATE_FAV = false;





    public void ReadDirByThread(Context context, Boolean read_dir)
    {
        //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();



        directory_handler = new Handler(Looper.myLooper());



        Runnable runnable = () ->
        {



                fullread_exe(  read_dir);

        };
        Thread_Full_and_Half_FAV =  new Thread(runnable);

        Thread_Full_and_Half_FAV.start();

    }





    void fullread_exe(Boolean read_dir)
    {

            if(zz_Config.TEST)
            {


                Log.d("_#FILL_DATA A", "StartUpScreenThread_dir_fill : aa_activity_home_screen A");
            }

            if(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER  && read_dir)
            aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.getVideo("aa_activity_home_screen");


            directories.clear();

            for (int i = 0; (!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER)  && (i <aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_fav.size()) ; i++)
            {

                List<String> values = new ArrayList<String>();
         //       values = aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.map.get(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_fav.get(i));
                //  Log.d("DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names_fav.get(i));

                values = aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_fav.get(i);

                DirectoryFav directoryFav = new DirectoryFav(values.get(0), values.get(1),values.get(2) + " Videos");
                directories.add(directoryFav);
            }



            if(zz_Config.TEST)
                Log.d("_#FILL_DATA B", "StartUpScreenThread_dir_fill : aa_activity_home_screen B" );



        Runnable r = new Runnable() {
            @Override
            public void run()
            {
/*
                public RecyclerView recyclerViewFav;
                public RecyclerAdapterFav recyclerAdapterFav;
                public List<DirectoryFav> directories =new ArrayList<>();
                public SwipeRefreshLayout mSwipeRefreshLayoutFav;

                */


                if(zz_Config.TEST)
                    Log.d("__#FILL_DATA -C", "StartUpScreenThread_dir_fill : aa_activity_home_screen A" );


                recyclerAdapterFav.setRecyclerAdapter(directories,getContext());
                recyclerAdapterFav.notifyItemRangeChanged(0, directories.size());
                recyclerAdapterFav.notifyDataSetChanged();
               // mSwipeRefreshLayoutFav.setRefreshing(false);
           //     aa_activity_home_screen.mContext.fileManager_home_fav.recyclerViewFav.setNestedScrollingEnabled(false);

                aa_activity_home_screen.mContext.fileManager_home_fav.recyclerViewFav.setLayoutFrozen(false);

               // aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names_fav.clear();

                // enable_tab(aa_activity_home_screen.mContext);
                  if(zz_Config.TEST)
                      Log.d("__#FILL_DATA -D", "Handler StartUpScreenThread_dir_fill : aa_activity_home_screen B" );

            }
        };


        while(!READY_TO_UPDATE_FAV);

        if(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )

             directory_handler.post(r);

        if(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )
           directory_handler.removeCallbacksAndMessages(null);

        if(zz_Config.TEST)
            Log.d("_#FILL_DATA E", "StartUpScreenThread_dir_fill : aa_activity_home_screen B" );



    }





    public FileManager_Home_fav() {
        this.directories = new ArrayList<>();
    }









}
