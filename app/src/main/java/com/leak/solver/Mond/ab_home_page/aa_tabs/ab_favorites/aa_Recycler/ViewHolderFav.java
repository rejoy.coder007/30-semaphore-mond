package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leak.solver.Mond.R;


public class ViewHolderFav extends RecyclerView.ViewHolder {


    public TextView dir_name;
    public TextView video_count;
    public LinearLayout slice;
    public LinearLayout slice1;

    public ViewHolderFav(@NonNull View itemView)
    {
        super(itemView);

        dir_name = itemView.findViewById(R.id.video_thumb);
        video_count = itemView.findViewById(R.id.file_name);
        slice  = itemView.findViewById(R.id.slice);
        slice1  = itemView.findViewById(R.id.slice1);
    }
}
