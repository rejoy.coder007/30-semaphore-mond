package com.leak.solver.Mond.ac_VideoPlayer;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.paracelable_Folder_VideoPlayer;
import com.leak.solver.Mond.zz_Config;


public   class VideoPlayer extends AppCompatActivity{

    private ContentResolver cResolver;
    private Window window;
    float currentvolume=0;
    float brightness=0;


    MyGestureListener myGestureListener;

    float brightness_default=0;

    boolean brightness_first_time=false;

    TextView center_vol;


    int SCALE_MODE_NEXT =1;
//btn_back
    Boolean hw_sw=false;
    aa_activity_home_screen  HOME_SCREEN=null;
    int CURRENT_INDEX =0;

    String videoUri;

    TextView video_title=null;


    DefaultTimeBar timeBar ;


    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    public SimpleExoPlayer player=null;
    ImageView vid_settings;
   // private SimpleExoPlayerView playerView;

    PlayerView playerView;

    private long playbackPosition;
    private int currentWindow;
    private boolean playWhenReady = true;




    ImageButton normal_scale;
    ImageButton fitwidth_scale;

    ImageButton fitwidthcrop_scale;



     AudioManager audioManager =null;
    int maxVol, curVol;




    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
           // initializePlayer();
        }
    }

    @Override
    public void onResume() {



         Display display;
         Point size;
        super.onResume();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
           // player.setVideoScalingMode(C.VIDEO_SCALING_MODE_DEFAULT);
          //  fullScreen(null);
            enable_button(1);


            display = getWindowManager().getDefaultDisplay();
            size = new Point();
            display.getSize(size);
            myGestureListener.sWidth = size.x;
            myGestureListener.sHeight = size.y;

            myGestureListener.videoPlayer=this;


        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {


            if(!brightness_first_time)
            setBrightness((int)brightness_default);

            int i=10;
            while(i<10)
            {
                i++;
                audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
            }



            releasePlayer();
        }
        //aa_activity_home_screen.mContext=null;




    }

    private void releasePlayer() {
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }

    private MediaSource buildMediaSource(Uri uri)
    {
        int type = Util.inferContentType(uri.getLastPathSegment());


        if(zz_Config.TEST)
        {


            Log.d("_##TYPE",  C.TYPE_SS+"::"+C.TYPE_DASH+"::"+C.TYPE_HLS+"::"+C.TYPE_OTHER+"---"+type);


        }




        switch (type)
        {

            /*
            case C.TYPE_SS:
                return new SsMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, null);
            case C.TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, null);
            case C.TYPE_HLS:
                return new HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, null);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource(uri, mediaDataSourceFactory, new DefaultExtractorsFactory(),
                        mainHandler, null);
                          */
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }

        }
    }

    private void buildMediaSource_org(Uri mUri) {


        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);



        player.prepare(videoSource);
        player.setPlayWhenReady(true);

    }



    @SuppressLint("InlinedApi")
    private void hideSystemUiFullScreen() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }


     private void initializePlayer()
    {








        enable_button(1);
        if (player == null)
        {



            TrackSelection.Factory factory = new AdaptiveTrackSelection.Factory();
            LoadControl loadControl = new DefaultLoadControl();

            DefaultRenderersFactory defaultRenderersFactory = new DefaultRenderersFactory(this);



            player = ExoPlayerFactory.newSimpleInstance(this,defaultRenderersFactory, new DefaultTrackSelector(factory), loadControl);




           playerView.setPlayer(player);
            player.setPlayWhenReady(playWhenReady);
           player.seekTo(currentWindow, playbackPosition);
        }
        buildMediaSource_org(Uri.parse(videoUri));








    }

    public  void onclick_next(View view){

        if(zz_Config.TEST)
        {


            Log.d("_##NEXT", "Clicked next/");


        }

    //    CURRENT_INDEX = (CURRENT_INDEX+1)%aa_activity_home_screen.mContext.fileManager_home_folder.directories_folder.size();

        CURRENT_INDEX = (CURRENT_INDEX+1)%HOME_SCREEN.fileManager_home_folder.directories_folder.size();

     //   Directory_folder directory_folder = aa_activity_home_screen.mContext.fileManager_home_folder.directories_folder.get(CURRENT_INDEX);

          Directory_folder directory_folder = HOME_SCREEN.fileManager_home_folder.directories_folder.get(CURRENT_INDEX);

        // videoUri = paracelable_folder_videoPlayer.getText1();
        videoUri = directory_folder.video_thumb;
        video_title.setText(directory_folder.getFile_name());

         initializePlayer();


    }


   void enable_button(int SM)
   {





           switch (SM)
           {
               case 0:{


                   normal_scale.setVisibility(View.VISIBLE);
                   fitwidth_scale.setVisibility(View.GONE);
                   fitwidthcrop_scale.setVisibility(View.GONE);



                   break;

               }


               case 1:{

                   normal_scale.setVisibility(View.GONE);
                   fitwidth_scale.setVisibility(View.VISIBLE);
                   fitwidthcrop_scale.setVisibility(View.GONE);



                   break;

               }



               case 2:{

                   normal_scale.setVisibility(View.GONE);
                   fitwidth_scale.setVisibility(View.GONE);
                   fitwidthcrop_scale.setVisibility(View.VISIBLE);

                   break;

               }
           }





   }



    @SuppressLint("WrongConstant")
    public  void fullScreen(View view) {




        normal_scale = findViewById(R.id.exo_f_a_normal);
        fitwidth_scale= findViewById(R.id.exo_f_b_full);

        fitwidthcrop_scale= findViewById(R.id.exo_f_c_crop);







        switch (SCALE_MODE_NEXT)
        {
            case 1:{




                SCALE_MODE_NEXT =2;

                playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT);

                break;

            }


            case 2:{

                SCALE_MODE_NEXT =0;
                playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);


                break;

            }



            case 0:{

                SCALE_MODE_NEXT =1;

                playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                player.setVideoScalingMode(C.VIDEO_SCALING_MODE_DEFAULT);

                break;

            }
        }








        enable_button(SCALE_MODE_NEXT);








        if(zz_Config.TEST)
        {


            Log.d("_##FULL", "Clicked next/");


        }



    }


    public  void onclick_back(View view) {

         onBackPressed();
    }

        public  void onclick_prev(View view){

        if(zz_Config.TEST)
        {


            Log.d("_##NEXT", "Clicked next/");


        }

        if(CURRENT_INDEX==0)
        {
            CURRENT_INDEX= aa_activity_home_screen.mContext.fileManager_home_folder.directories_folder.size()-1;
        }
        else

        CURRENT_INDEX = (CURRENT_INDEX-1)%aa_activity_home_screen.mContext.fileManager_home_folder.directories_folder.size();


        Directory_folder directory_folder = aa_activity_home_screen.mContext.fileManager_home_folder.directories_folder.get(CURRENT_INDEX);
        // videoUri = paracelable_folder_videoPlayer.getText1();
        videoUri = directory_folder.video_thumb;

        video_title.setText(directory_folder.getFile_name());


        initializePlayer();

        // videoUri = directory_folder.video_thumb;


    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {



        super.onCreate(savedInstanceState);
        //setContentView(R.layout.zz_exo_video_player);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.zy_activity_video_player);

        playerView =  findViewById(R.id.player_view);

        video_title = findViewById(R.id.txt_title);
        center_vol = findViewById(R.id.center_vol);

        center_vol.setVisibility(View.GONE);

        vid_settings = findViewById(R.id.vid_more);


        timeBar = findViewById(R.id.exo_progress);

        timeBar.setPlayedColor(Color.BLUE);


          brightness_default=0;

          brightness_first_time=true;


         audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        // maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);

     //   currentvolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);


        if(zz_Config.TEST)
        {


            Log.d("__VOLD",""+currentvolume+"::"+maxVol );


        }



        normal_scale = findViewById(R.id.exo_f_a_normal);
         fitwidth_scale= findViewById(R.id.exo_f_b_full);

         fitwidthcrop_scale= findViewById(R.id.exo_f_c_crop);




        Intent intent = getIntent();



        paracelable_Folder_VideoPlayer  paracelable_folder_videoPlayer = intent.getParcelableExtra("VideoData");
        HOME_SCREEN = aa_activity_home_screen.mContext;
       // aa_activity_home_screen.mContext=null;
        CURRENT_INDEX = paracelable_folder_videoPlayer.CurrentIndex;

        Directory_folder directory_folder =HOME_SCREEN.fileManager_home_folder.directories_folder.get(CURRENT_INDEX);
       // videoUri = paracelable_folder_videoPlayer.getText1();
        videoUri = directory_folder.video_thumb;
        if(zz_Config.TEST)
        {


           // Log.d("_##YES1","from intent"+HOME_SCREEN+CURRENT_INDEX+"::"+aa_activity_home_screen.mContext  );


        }

        video_title.setText(directory_folder.getFile_name());


        vid_settings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(VideoPlayer.this, vid_settings);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.context_menu_more_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                VideoPlayer.this,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;
                    }
                });

                popup.show(); //showing popup menu

            }
        });//closing the setOnClickListener method






        myGestureListener = new MyGestureListener();

        GestureDetector mDetector = new GestureDetector(VideoPlayer.this, myGestureListener);

        playerView.setOnTouchListener(new View.OnTouchListener()
        {


            float prevX = -1;
            float prevY = -1;
            @Override
            public boolean onTouch(final View v, final MotionEvent event)
            {
               // Log.d("__TAG", "UP LEFT");

                if(event.getRawX()<(myGestureListener.sWidth/2))
                {

                    myGestureListener.STATUS=0;
                }
                else
                {
                    myGestureListener.STATUS=1;
                }


                if(zz_Config.TEST) {

                    Log.d("__TAG", "Width ::"+myGestureListener.sWidth+"::"+myGestureListener.STATUS+"::"+event.getRawX()+"<"+myGestureListener.sWidth/2);

                }


                    if (prevX != -1 && prevY != -1)
                {
                    if (event.getX() > prevX)
                    {

                  //      Log.i("__TAG", "LR");


                          //Log.d("DEBUG", MotionEvent.ACTION_MOVE + ":" + event.getAction() + ":" + event.getActionMasked() + ":Left Swipe" + ":" + prevX + ":" + event.getX() + ":" + viewPager.getCurrentItem());
                    }
                    else if (prevX > event.getX())
                    {
                        // Right to left swipe

                   //     Log.i("__TAG", "RL");
                        //Log.d("DEBUG", MotionEvent.ACTION_MOVE + ":" + event.getAction() + ":" + event.getActionMasked() + ":Right Swipe" + ":" + prevX + ":" + event.getX() + ":" + viewPager.getCurrentItem());
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_MOVE)
                {



                    Direction direction = getDirection(prevX,prevY,event.getX(),event.getY());

                    prevX = event.getX();
                    prevY = event.getY();


                    switch(direction)
                    {

                        case up: {

                            if(zz_Config.TEST)
                            {

                                Log.d("__TAG", "UP  ");

                                /*
                                if (myGestureListener.STATUS <= 0)
                                    Log.d("__TAG", "UP LEFT");
                                else
                                    Log.d("__TAG", "UP RIGHT");

                                    */









                            }

                            if(myGestureListener.STATUS>0)
                            {


                                center_vol.setVisibility(View.VISIBLE);

                                /*
                                currentvolume = player.getVolume()+player.getVolume()* zz_Config.SOUND_RANGE;
                                // if(currentvolume>=0)

                                // Log.d("__TAG", "swipe down"+currentvolume);

*/

                                currentvolume=currentvolume +audioManager.getStreamVolume(AudioManager.STREAM_RING)* zz_Config.SOUND_RANGE;

                                if(currentvolume>=maxVol)
                                {

                                    currentvolume=maxVol;
                                }

                              //  player.setVolume(currentvolume);

                              //  audioManager.setStreamVolume(AudioManager.STREAM_RING, (int)currentvolume, 0);

                                audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);

                                if(zz_Config.TEST)
                                {
                                    Log.d("__TAG", "SOUND "+currentvolume);
                                }


                                center_vol.setText(""+  Math.round(currentvolume*100));

                                if(zz_Config.TEST)
                                {


                                    Log.d("__VOLC",""+currentvolume );


                                }

                            }
                            else
                            {

                                center_vol.setVisibility(View.GONE);
                                boolean settingsCanWrite = Settings.System.canWrite(VideoPlayer.this);

                                if(!settingsCanWrite) {
                                    Toast.makeText(VideoPlayer.this, "Require Permission to Handle Screen Brightness", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                                    startActivityForResult(intent, 200);

                                }
                                else
                                {



                                    changeScreenBrightness(VideoPlayer.this,zz_Config.brightnessSpeed);
                                }


                            }


                            break;
                        }


                        case down: {


                            Log.d("__TAG", "DOWN");
/*
                            if(zz_Config.TEST) {
                                if (myGestureListener.STATUS <= 0)
                                    Log.d("__TAG", "down LEFT");
                                else
                                    Log.d("__TAG", "down RIGHT");
                            }

                            */




                            if(myGestureListener.STATUS>0)
                            {

                                center_vol.setVisibility(View.VISIBLE);


                                /*

                                currentvolume = player.getVolume();

                                currentvolume=currentvolume -player.getVolume()* zz_Config.SOUND_RANGE;



*/
                                currentvolume=currentvolume -audioManager.getStreamVolume(AudioManager.STREAM_RING)* zz_Config.SOUND_RANGE;

                                if(currentvolume<=0)
                                {

                                    // if(currentvolume>=0)

                                    // Log.d("__TAG", "swipe down"+currentvolume);
                                    currentvolume=0;
                                }
                                else
                                {
                                  //  player.setVolume(currentvolume);
                                   // audioManager.setStreamVolume(AudioManager.STREAM_RING, (int)currentvolume, 0);
                                }

                                audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);





                                if(zz_Config.TEST)
                                {
                                    Log.d("__TAG", "SOUND "+currentvolume);
                                }
                            }
                            else
                            {

                                center_vol.setVisibility(View.GONE);
                                boolean settingsCanWrite = Settings.System.canWrite(VideoPlayer.this);

                                if(!settingsCanWrite)
                                {
                                    Toast.makeText(VideoPlayer.this, "Require Permission to Handle Screen Brightness", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                                    startActivityForResult(intent, 200);

                                }
                                else
                                {

                                    changeScreenBrightness(VideoPlayer.this,-zz_Config.brightnessSpeed);
                                }


                            }

                            center_vol.setText(""+  Math.round(currentvolume*100));

                            if(zz_Config.TEST)
                            {


                                Log.d("__VOLC",""+currentvolume );


                            }

                            break;
                        }



                        case left: {

                            if(zz_Config.TEST)
                                Log.d("__TAG", "LEFT");

                            break;
                        }



                        case right: {

                            if(zz_Config.TEST)
                                Log.d("__TAG", "RIGHT");

                            break;
                        }


                    }




                }
                else
                {
                    prevX = -1;
                    prevY = -1;
                    center_vol.setVisibility(View.GONE);
                }


                return mDetector.onTouchEvent(event);
            }
        });





    }




    public void changeScreenBrightness(Context context,   double brightnessSpeed)
    {
        // Change the screen brightness change mode to manual.
      //  Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        // Apply the screen brightness value to the system, this will change the value in Settings ---> Display ---> Brightness level.
        // It will also change the screen brightness for the device.


    /*
        int screenBrightnessValue = 0;
        try {
            screenBrightnessValue = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            Log.i("__TAG", "error");
        }
      //  Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, screenBrightnessValue);




        Window window = getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.screenBrightness = screenBrightnessValue / 255f;
        window.setAttributes(layoutParams);

*/

        cResolver = getContentResolver();
        window = getWindow();
        try
        {
            Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);



            if(brightness_first_time)
            {
                brightness_first_time=false;
                brightness_default=brightness;

            }
        }
        catch (Settings.SettingNotFoundException e)
        {
            e.printStackTrace();
            Log.i("__TAG", "error");
        }





        int new_brightness = (int) (brightness + (brightness * brightnessSpeed));
        if (new_brightness > 250)
        {
            new_brightness = 250;
        }
        else if (new_brightness < 1)
        {
            new_brightness = 1;
        }




        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, (new_brightness));
        WindowManager.LayoutParams layoutpars = window.getAttributes();
        layoutpars.screenBrightness = brightness / (float) 255;
        window.setAttributes(layoutpars);

        Log.i("__TAG", "bright");


    }

    public void setBrightness(int brightness_val)
    {







        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, (brightness_val));
        WindowManager.LayoutParams layoutpars = window.getAttributes();
        layoutpars.screenBrightness = brightness / (float) 255;
        window.setAttributes(layoutpars);

        Log.i("__TAG", "bright");

    }


    public double getAngle(float x1, float y1, float x2, float y2) {

        double rad = Math.atan2(y1-y2,x2-x1) + Math.PI;
        return (rad*180/Math.PI + 180)%360;
    }


    /**
     * Given two points in the plane p1=(x1, x2) and p2=(y1, y1), this method
     * returns the direction that an arrow pointing from p1 to p2 would have.
     * @param x1 the x position of the first point
     * @param y1 the y position of the first point
     * @param x2 the x position of the second point
     * @param y2 the y position of the second point
     * @return the direction
     */
    public Direction getDirection(float x1, float y1, float x2, float y2){
        double angle = getAngle(x1, y1, x2, y2);
        return Direction.fromAngle(angle);
    }


    public enum Direction{
        up,
        down,
        left,
        right;

        /**
         * Returns maxVol direction given an angle.
         * Directions are defined as follows:
         *
         * Up: [45, 135]
         * Right: [0,45] and [315, 360]
         * Down: [225, 315]
         * Left: [135, 225]
         *
         * @param angle an angle from 0 to 360 - e
         * @return the direction of an angle
         */
        public static Direction fromAngle(double angle){
            if(inRange(angle, 45, 135)){
                return Direction.up;
            }
            else if(inRange(angle, 0,45) || inRange(angle, 315, 360)){
                return Direction.right;
            }
            else if(inRange(angle, 225, 315)){
                return Direction.down;
            }
            else{
                return Direction.left;
            }

        }

        /**
         * @param angle an angle
         * @param init the initial bound
         * @param end the final bound
         * @return returns true if the given angle is in the interval [init, end).
         */
        private static boolean inRange(double angle, float init, float end){
            return (angle >= init) && (angle < end);
        }
    }




}
