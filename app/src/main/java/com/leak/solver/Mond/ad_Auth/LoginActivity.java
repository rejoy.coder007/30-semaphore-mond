package com.leak.solver.Mond.ad_Auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_activity_home_screen;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {

    FirebaseUser current_user;

    Button LoginButton,PhoneButton;
    EditText UserEmail,UserPassword;
    TextView New_account_needed,ForgetPassword;

    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ae_activity_login);
        mAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);

        LoginButton = (Button)findViewById(R.id.login_button);
        PhoneButton = (Button)findViewById(R.id.login_button);

        UserEmail  = (EditText) findViewById(R.id.login_email);
        UserPassword  = (EditText) findViewById(R.id.password_email);

        New_account_needed = (TextView) findViewById(R.id.need_new_account);
        ForgetPassword = (TextView) findViewById(R.id.forgetPassword);

        New_account_needed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToRegisterActivity();
            }
        });


        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginAccount();
            }
        });





    }

    @Override
    protected void onStart() {
        super.onStart();
        if(current_user!=null)
        {
            SendUserToHome();
        }
    }

    void  SendUserToHome(){

        Intent intent = new Intent(LoginActivity.this, aa_activity_home_screen.class);

        startActivity(intent);



    }


    void  SendUserToRegisterActivity(){

        Intent intent = new Intent(LoginActivity.this, Register.class);

        startActivity(intent);



    }


    void LoginAccount(){

        //Intent intent = new Intent(Register.this, LoginActivity.class);

        progressDialog.setTitle("Login");
        progressDialog.setMessage("Please wait while we are signing account");
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.show();

        // startActivity(intent);
        String Email = UserEmail.getText().toString();
        String Password = UserPassword.getText().toString();

        if(TextUtils.isEmpty(Email)){
            Toast.makeText(getApplicationContext(),"Please Enter Email", Toast.LENGTH_SHORT).show();

        }
        else
        {


            if(TextUtils.isEmpty(Password)){
                Toast.makeText(getApplicationContext(),"Please Enter Password", Toast.LENGTH_SHORT).show();

            }
            else
            {

                mAuth.signInWithEmailAndPassword(Email, Password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task)
                            {
                                if (task.isSuccessful())
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(),"Login Success", Toast.LENGTH_SHORT).show();
                                    onBackPressed();
                                }
                                else
                                {

                                    String message = task.getException().toString();
                                    Toast.makeText(getApplicationContext(),"Error :: "+message, Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }

                                // ...
                            }
                        });

                // ...
            }






        }


    }
}
