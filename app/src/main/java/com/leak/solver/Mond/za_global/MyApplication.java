package com.leak.solver.Mond.za_global;

import android.app.ActivityManager;
import android.app.Application;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.util.Log;

//import com.crashlytics.android.Crashlytics;
//import com.instabug.library.Instabug;
//import com.instabug.library.invocation.InstabugInvocationEvent;
import com.leak.solver.Mond.zz_Config;
import com.squareup.leakcanary.AndroidExcludedRefs;
import com.squareup.leakcanary.DisplayLeakService;
import com.squareup.leakcanary.ExcludedRefs;
import com.squareup.leakcanary.LeakCanary;

//import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyApplication extends Application
{


    @Override
    public void onCreate()
    {
        super.onCreate();

        if(zz_Config.TEST)
        {
            Log.d("_#_ON _CREATE_APP", "onCreate: MyApplication");

        }



        if (LeakCanary.isInAnalyzerProcess(this))
        {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                .instanceField("android.view.inputmethod.InputMethodManager", "sInstance")
                .instanceField("android.view.inputmethod.InputMethodManager", "mLastSrvView")
                .instanceField("com.android.internal.policy.PhoneWindow$DecorView", "mContext")
                .instanceField("android.support.v7.widget.SearchView$SearchAutoComplete", "mContext")
                .build();


/*
        LeakCanary.refWatcher(this)
                .listenerServiceClass(DisplayLeakService.class)
              .excludedRefs(excludedRefs)
                .buildAndInstall();
*/
         LeakCanary.install(this);

        //LeakCanary.install(this);

        /*
        new Instabug.Builder(this, "46385c033cf94b747eb8c7dd4126e8c2")
                .setInvocationEvents(InstabugInvocationEvent.SHAKE, InstabugInvocationEvent.SCREENSHOT)
                .build();*/

      //  Fabric.with(this, new Crashlytics());

        // Normal app init code...
    }




/*

ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                    .instanceField("android.view.inputmethod.InputMethodManager", "sInstance")
                    .instanceField("android.view.inputmethod.InputMethodManager", "mLastSrvView")
                    .instanceField("com.android.internal.policy.PhoneWindow$DecorView", "mContext")
                    .instanceField("android.support.v7.widget.SearchView$SearchAutoComplete", "mContext")
                    .build();

            LeakCanary.refWatcher(this)
                    .listenerServiceClass(DisplayLeakService.class)
                    .excludedRefs(excludedRefs)
                    .buildAndInstall();
 */







}

