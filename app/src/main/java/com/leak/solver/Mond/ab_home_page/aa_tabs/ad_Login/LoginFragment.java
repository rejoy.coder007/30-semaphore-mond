package com.leak.solver.Mond.ab_home_page.aa_tabs.ad_Login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.zz_Config;

public class LoginFragment extends Fragment {


    public LoginFragment() {
        // Required empty public constructor
    }


    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

        if(zz_Config.TEST)
        {
            Log.d("_#_SPACE_FRAG_ON_CREATE", "FRAGMENT SPACE ON_CREATE   : aa_activity_home_screen__ "+mPage);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView=null;


        rootView = inflater.inflate(R.layout.ae_activity_login, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.share_space);
       // textView.setText(textView.getText()+" :: Fragment #" + mPage);


        if(zz_Config.TEST)
        {
            Log.d("_#_SPACE_FRAG__ON_VIEW", "FRAGMENT SPACE VIRECREATE   : aa_activity_home_screen__ "+mPage);

        }
         return rootView;
    }


    @Override
    public void onStop() {

        if(zz_Config.TEST)
        {
            Log.d("&_@@_#SPC_FRAG_STP", " VISIBLE folder fragment  : aa_activity_home_screen__ "+mPage+"::");

        }





        Runtime.getRuntime().gc();


        super.onStop();  // Always call the superclass method first

    }







}
