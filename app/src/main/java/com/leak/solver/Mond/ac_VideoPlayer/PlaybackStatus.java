package com.leak.solver.Mond.ac_VideoPlayer;

public class PlaybackStatus {
    //Minimum Video you want to buffer while Playing
    public static final int MIN_BUFFER_DURATION = 3000;
    //Max Video you want to buffer during PlayBack
    public static final int MAX_BUFFER_DURATION = 5000;
    //Min Video you want to buffer before start Playing it
    public static final int MIN_PLAYBACK_START_BUFFER = 1500;
    //Min video You want to buffer when user resumes video
    public static final int MIN_PLAYBACK_RESUME_BUFFER = 5000;

    public static final String DEFAULT_VIDEO_URL = "https://androidwave.com/media/androidwave-video-3.mp4";


    public static  final  int LOADING =1;
    public static  final  int STOPPED =2;
    public static  final  int IDLE =3;
    public static  final  int PLAYING =4;
    public static  final  int PAUSED =5;
}
