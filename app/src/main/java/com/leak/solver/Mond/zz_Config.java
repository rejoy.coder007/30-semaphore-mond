package com.leak.solver.Mond;

import android.app.ActivityManager;

import java.util.List;


// TIME_DELAY_INSIDE_THREAD_START
// TIME_DELAY_INSIDE_THREAD_START
public class zz_Config {

   public static long TIME_DELAY_INSIDE_THREAD_START =0;
   public static long TIME_DELAY_INSIDE_THREAD_HOME =0;
   public static long TIME_DELAY_START_SCREEN = 4000;
   public static long TIME_DELAY_START_SCREEN_STEP = 20;
    //public  static long TIME_DELAY_FLASH_SCREEN = 1000;
    public static int STRING_TRIM             = 735;

    public static Boolean IS_BACK             = false;
    public static Boolean TEST             = true;
    static String                                               name_file=  "com.leak.solver.Mond.";


    public static  double brightnessSpeed =20.05;
    public static float SOUND_RANGE =.05f;

    public static String getAppTaskState(ActivityManager activityManager)
    {



        StringBuilder stringBuilder=new StringBuilder();
        int totalNumberOfTasks=activityManager.getRunningTasks(10).size();//Returns total number of tasks - stacks
        stringBuilder.append("\nTotal Number of Tasks: "+totalNumberOfTasks+"\n\n");

        List<ActivityManager.RunningTaskInfo> taskInfo =activityManager.getRunningTasks(10);//returns List of RunningTaskInfo - corresponding to tasks - stacks

        for(ActivityManager.RunningTaskInfo info:taskInfo){
            stringBuilder.append("Task Id: "+info.id+", Number of Activities : "+info.numActivities+"\n");//Number of Activities in task - stack

            // Display the root Activity of task-stack
            stringBuilder.append("TopActivity: "+info.topActivity.getClassName().
                    toString().replace(name_file,"")+"\n");

            // Display the top Activity of task-stack
            stringBuilder.append("BaseActivity:"+info.baseActivity.getClassName().
                    toString().replace(name_file,"")+"\n");
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }





}
