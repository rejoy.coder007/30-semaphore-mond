package com.leak.solver.Mond.ab_home_page;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.leak.solver.Mond.R;
import com.leak.solver.Mond.aa_StartUp.aa_activity_StartUp;

import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.FileManager_Home_fav;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.FileManager_Home_folder;
import com.leak.solver.Mond.ac_VideoPlayer.VideoPlayer;
import com.leak.solver.Mond.ad_Auth.LoginActivity;
import com.leak.solver.Mond.zz_Config;

/*adb push ZZ_3gp_JOJO  /storage/emulated/0
*/

public class aa_activity_home_screen extends AppCompatActivity {


    private FirebaseUser current_user=null;
    private FirebaseAuth mAuth;


    Menu menu_Final=null;


    public static aa_activity_home_screen                                            mContext=null;

    public  aa_activity_home_screen                                            MYHOME_ACTVITY=null;

    public aa_activity_StartUp                                                _global_StartUP =null;

    public FileManager_Home_fav                                           fileManager_home_fav=null;
    public FileManager_Home_folder fileManager_home_folder=null;

    public FileManager_Home                                                         fileManager_home=null;


    // 1 -- already 2 --after allow
    public int STATUS_RESUME =0;


  public  boolean BY_iNTENT=false;

    public static aa_activity_home_screen getContext() {
        return mContext;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_activity_aa_home_screen);

        mAuth = FirebaseAuth.getInstance();

        MYHOME_ACTVITY =this;
      //  IMMLeaks.fixFocusedViewLeak(getApplication());
        /////////////////////////Initialization /////////////////////

        /*
        _global_StartUP = aa_activity_StartUp.aaStartUp.Get_obj();
        fileManager_home = new FileManager_Home();
        fileManager_home_fav = new FileManager_Home_fav();

        fileManager_home_folder = new FileManager_Home_folder();

        Thread_Init();
        */
       // fileManager_home_fav.enable_tab(aa_activity_home_screen.this);

       // fileManager_home_fav.enable_tab(aa_activity_home_screen.this);

        //  handler = new Handler(Looper.myLooper());



        if(zz_Config.TEST)
        {
            Log.d("_####_ON _CREATE", "onCreate: aa_activity_home_screen -- A");
        }



        fileManager_home = new FileManager_Home();
        fileManager_home_fav = new FileManager_Home_fav();
        fileManager_home_folder = new FileManager_Home_folder();
        fileManager_home.window_toolbar_navbar_bg(aa_activity_home_screen.this);

        _global_StartUP = aa_activity_StartUp.aaStartUp.Get_obj();
        fileManager_home.reset_variables(aa_activity_home_screen.this);
        fileManager_home.initialize(aa_activity_home_screen.this);
        fileManager_home.getPermission(aa_activity_home_screen.this);

        fileManager_home.enable_tab(aa_activity_home_screen.this);

        fileManager_home.disable_tab(2,1);
        fileManager_home.CURRENT_TAB=1;










//

        if(zz_Config.TEST)
            Log.d("_###_ADAPTER_THREAD", " Hash Inside AdapterThread :   HomePage"+ aa_activity_StartUp.aaStartUp);


        // func_test();

/*
        directories = new ArrayList<>();

        for (int i = 0; i <  0; i++)
        {

              DirectoryFav directoryFav = new DirectoryFav( "lol"+i, i + " Videos");
            directories.add(directoryFav);
        }

        if(zz_Config.TEST)
        {
            Log.d("_###ADAPTER_THREAD", " Hash Inside AdapterThread :   HomePage"+ Arrays.toString(directories.toArray()));

        }
 */

       // enable_tab();







        if(zz_Config.TEST)
        {
            Log.d("_####_ON _CREATE", "onCreate: aa_activity_home_screen -- B");

        }

        // Runtime.getRuntime().gc();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {



        if(zz_Config.TEST)
        {
            Log.d("_####_ON_MENU 1 ", "onCreateOptionsMenu");
        }


        //  getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);



        current_user=mAuth.getCurrentUser();


        MenuInflater infl = getMenuInflater();
        infl.inflate(R.menu.main_menu, menu);
        /*
        menu_Final = menu;


        if(menu!=null)
        {
            MenuItem item = menu.getItem(1);


            if(current_user==null)
            {
                SpannableString spanString = new SpannableString("Login");
                int end = spanString.length();
                spanString.setSpan(new RelativeSizeSpan(1.0f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                item.setTitle(spanString);

            }
            else
            {
                SpannableString spanString = new SpannableString("Logout");
                int end = spanString.length();
                spanString.setSpan(new RelativeSizeSpan(1.0f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                item.setTitle(spanString);
            }

        }
*/






        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {




        int id = item.getItemId();

        if (id == R.id.login_out) {



/*

                     SpannableString spanString = new SpannableString("Login");
                     int end = spanString.length();
                     spanString.setSpan(new RelativeSizeSpan(1.0f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                     item.setTitle(spanString);
*/
                       mAuth.signOut();

            fileManager_home.SendUserToLogin();







            Toast.makeText(getApplicationContext(), "item1 is selected", Toast.LENGTH_SHORT).show();

        }



        return super.onOptionsItemSelected(item);

    }


    @Override
    protected void onResume()
    {

        if(zz_Config.TEST)
        {
            Log.d("_####_ON_MENU 2 ", "onResume");
        }


        /*
       if(menu_Final!=null)
       {
           MenuItem item = menu_Final.getItem(1);


           if(current_user==null)
           {
               SpannableString spanString = new SpannableString("Login");
               int end = spanString.length();
               spanString.setSpan(new RelativeSizeSpan(1.0f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


               item.setTitle(spanString);

               if(zz_Config.TEST)
               {
                   Log.d("_####_ON_MENU 3 ", "onResume");
               }

           }
           else
           {
               SpannableString spanString = new SpannableString("Logout");
               int end = spanString.length();
               spanString.setSpan(new RelativeSizeSpan(1.0f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
               item.setTitle(spanString);


               if(zz_Config.TEST)
               {
                   Log.d("_####_ON_MENU 4 ", "onResume");
               }
           }

       }
*/

        BY_iNTENT=false;

        if(zz_Config.TEST)
        {
            Log.d("_####_ON_RESUME", "onResume: aa_activity_home_screen -- A");
        }


        fileManager_home.initialize(aa_activity_home_screen.this);



       //                   aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread_folder(appCompatActivity,true);

        if(STATUS_RESUME>0)
        {


            switch(STATUS_RESUME)
            {
                case 1 :


                      switch (fileManager_home.CURRENT_TAB)
                      {
                          case 0: break;
                          case 1:
                              aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread(aa_activity_home_screen.this,false);

                              break;
                          case 2: break;
                      }






                    break;
                case 2 :


                    switch (fileManager_home.CURRENT_TAB)
                    {
                        case 0: break;
                        case 1:
                            aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread(aa_activity_home_screen.this,true);

                            break;
                        case 2: break;
                    }




                    break;
                default:break;
            }

        }


/*

        fileManager_home.initialize(aa_activity_home_screen.this);

        if (ActivityCompat.checkSelfPermission(aa_activity_home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

         //   aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread_folder(aa_activity_home_screen.this);
            if(need_reload_dir)
            {
                   aa_activity_home_screen.mContext.fileManager_home_fav.ReadDirByThread_folder(aa_activity_home_screen.this);

            }
            else
                need_reload_dir=false;


        }

*/


        super.onResume();

        /*
        if(zz_Config.TEST)
        {
            Log.d("_####_ON_RESUME", "onResume: aa_activity_home_screen -- B");
            Log.d("_#APP_STATE_H",zz_Config.getAppTaskState(aa_activity_StartUp.aaStartUp.activityManager));

        }
*/


    }

    @Override
    protected void onUserLeaveHint()
    {


        if(zz_Config.TEST){
            Log.d("_####_ON_LEAVE", "onUserLeaveHint  :  aa_activity_home_screen A");

        }
        STATUS_RESUME =2;




        switch (fileManager_home.CURRENT_TAB)
        {
            case 0 : break;
            case 1 :

                if(aa_activity_home_screen.mContext.fileManager_home_fav.recyclerViewFav!=null)
                {
                    aa_activity_home_screen.mContext.fileManager_home_fav.recyclerViewFav.setLayoutFrozen(true);

                }

                break;


            case 2 :



                break;
        }



        fileManager_home.reset_variables(aa_activity_home_screen.this);



  /*



        if (ActivityCompat.checkSelfPermission(aa_activity_home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

           Thread_Kiler(null);
            need_reload_dir=true;




        }
        else
        {
            fileManager_home.reset_variables(aa_activity_home_screen.this);
        }


 */
           super.onUserLeaveHint();


    }


    @Override
    protected void onStop()
    {

        if(zz_Config.TEST)
        {
            Log.d("_####_ON_LEAVE_S", "onStop  :  aa_activity_home_screen A");

        }

        fileManager_home.initialize(aa_activity_home_screen.this);
        fileManager_home.Thread_Kiler();
        fileManager_home.reset_variables(aa_activity_home_screen.this);


        super.onStop();


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


     //   setContentView(R.layout.ac_activity_aa_home_screen);

     //   fileManager_home.enable_tab(aa_activity_home_screen.this);

    //    fileManager_home.disable_tab(2,1);
     //   fileManager_home.CURRENT_TAB=1;


    }



        @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {

        Log.d("_####_ON_AR", "onResume: aa_activity_home_screen -- A");

        fileManager_home.initialize(aa_activity_home_screen.this);


        fileManager_home.onRequestPermissionsResult(aa_activity_home_screen.this,requestCode,  permissions, grantResults);

        Log.d("_####_ON_AR", "onResume: aa_activity_home_screen -- A");



    }



    @Override
    public void onBackPressed()
    {

        if(zz_Config.TEST)
            Log.d("_####_BACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen A");




        switch (fileManager_home.CURRENT_TAB)
        {
            case 0 :
            case 1 :

                fileManager_home.Thread_Kiler( );

                while (getFragmentManager().getBackStackEntryCount() > 0)
                {
                    getFragmentManager().popBackStack(); // pop fragment here
                }
                zz_Config.IS_BACK=true;
                aa_activity_home_screen.mContext=null;


                super.onBackPressed(); // after nothing is there default behavior of android works.

                break;


            case 2 :

                fileManager_home.disable_tab(2,1);
                fileManager_home.CURRENT_TAB=1;

                break;
        }


        if(zz_Config.TEST)
            Log.d("_####_BACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen B");


    }


    public  void c()
    {
        Intent intent = new Intent(aa_activity_home_screen.this, VideoPlayer.class);
         startActivity(aa_activity_home_screen.mContext.fileManager_home.intent);

    }


    @Override
    protected void onStart() {
        super.onStart();



        if(current_user==null)
        {
          //SendUserToLogin();
        }


        //  Intent intent = new Intent(aa_activity_home_screen.mContext, LoginActivity.class);

      //  aa_activity_home_screen.mContext.startActivity(intent);

/*
        Intent intent = new Intent(aa_activity_home_screen.mContext, LoginActivity.class);


        if(zz_Config.TEST)
        {


            //   Log.d("_##YES","from intent"+paracelableFolderVideoPlayer.mContext+paracelableFolderVideoPlayer.CurrentIndex  );


        }
        aa_activity_home_screen.getContext().BY_iNTENT=true;

        aa_activity_home_screen.getContext().startActivity(intent);
*/

    }

    void  SendUserToLogin(){

        Intent intent = new Intent(aa_activity_home_screen.this, LoginActivity.class);

        startActivity(intent);



    }



}
