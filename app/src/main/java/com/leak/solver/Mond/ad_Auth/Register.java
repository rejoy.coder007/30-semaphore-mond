package com.leak.solver.Mond.ad_Auth;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.leak.solver.Mond.R;

public class Register extends AppCompatActivity {
    Button CreateAccount;
    TextView AlreadyRegistered;
    EditText UserEmail,UserPassword;

    private FirebaseAuth mAuth;

    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.af_activity_register);

        progressDialog = new ProgressDialog(this);
     //   FirebaseApp.initializeApp(Register.this);
        mAuth = FirebaseAuth.getInstance();


        CreateAccount = (Button)findViewById(R.id.register_button);


        UserEmail  = (EditText) findViewById(R.id.register_email);
        UserPassword  = (EditText) findViewById(R.id.password_register);

        AlreadyRegistered = (TextView) findViewById(R.id.alreadyaccount);
        AlreadyRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToLoginActivity();
            }
        });


        CreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 CreateNewAccount();
            }
        });
    }

    void SendUserToLoginActivity(){

        //Intent intent = new Intent(Register.this, LoginActivity.class);

       // startActivity(intent);


        onBackPressed();



    }


    void CreateNewAccount(){

        //Intent intent = new Intent(Register.this, LoginActivity.class);

        // startActivity(intent);
         String Email = UserEmail.getText().toString();
         String Password = UserPassword.getText().toString();

         if(TextUtils.isEmpty(Email)){
             Toast.makeText(getApplicationContext(),"Please Enter Email", Toast.LENGTH_SHORT).show();

         }
         else
         {


             if(TextUtils.isEmpty(Password)){
                 Toast.makeText(getApplicationContext(),"Please Enter Password", Toast.LENGTH_SHORT).show();

             }
             else
             {

                 progressDialog.setTitle("Creating User Account");
                 progressDialog.setMessage("Please wait while we are creating account");
                 progressDialog.setCanceledOnTouchOutside(true);
                 progressDialog.show();


                 mAuth.createUserWithEmailAndPassword(Email, Password)
                         .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                             @Override
                             public void onComplete(@NonNull Task<AuthResult> task) {
                                 if (task.isSuccessful())
                                 {
                                     progressDialog.dismiss();
                                     Toast.makeText(getApplicationContext(),"Account Creation Success", Toast.LENGTH_SHORT).show();
                                      onBackPressed();
                                 }
                                 else
                                 {

                                     String message = task.getException().toString();
                                     Toast.makeText(getApplicationContext(),"Error :: "+message, Toast.LENGTH_SHORT).show();
                                     progressDialog.dismiss();
                                 }

                                 // ...
                             }
                         });


             }


         }


    }



    @Override
    public void onBackPressed()
    {



                super.onBackPressed(); // after nothing is there default behavior of android works.



    }


}
